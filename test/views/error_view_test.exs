defmodule ProductWarehouse.ErrorViewTest do
  use ProductWarehouse.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 404.json" do
    assert render(ProductWarehouse.ErrorView, "404.json", []) ==
           %{errors: [%{message: "Page not found"}]}
  end

  test "render 500.json" do
    assert render(ProductWarehouse.ErrorView, "500.json", []) ==
           %{errors: [%{message: "Internal server error"}]}
  end

  test "render any other" do
    assert render(ProductWarehouse.ErrorView, "505.json", []) ==
           %{errors: [%{message: "Internal server error"}]}
  end
end
