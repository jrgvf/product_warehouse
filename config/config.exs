# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :product_warehouse,
  ecto_repos: [ProductWarehouse.Repo]

# Configures the endpoint
config :product_warehouse, ProductWarehouse.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "i9bRGhFS5nTie2NBR0J8f7X9okz5Ey4Yt2kNhFM4jOaQho4m8xrgM3GQdEH5mkiR",
  render_errors: [view: ProductWarehouse.ErrorView, accepts: ~w(json)],
  pubsub: [name: ProductWarehouse.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
