defmodule ProductWarehouse.Repo.Migrations.CreateProduct do
  use Ecto.Migration

  def change do
    create table(:products, primary_key: false) do
      add :uuid, :uuid, primary_key: true
      add :tenant, :string
      add :sku, :string
      add :description, :string
      add :qty, :integer
      add :price_from, :float
      add :price_to, :float
      add :ean, :string

      timestamps()
    end

    create unique_index(:products, [:tenant, :sku], name: :unique_sku_by_tenant)
    create constraint(:products, :price_from_must_be_positive, check: "price_from > 0")
    create constraint(:products, :price_to_must_be_positive, check: "price_to > 0")
    create constraint(:products, :price_reduction, check: "price_from >= price_to")
  end
end
