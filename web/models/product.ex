defmodule ProductWarehouse.Product do
  use ProductWarehouse.Web, :model

  alias ProductWarehouse.{Product, Repo}

  @timestamps_opts [type: :utc_datetime, usec: true]
  @primary_key {:uuid, :binary_id, [autogenerate: true]}

  schema "products" do
    field :tenant, :string
    field :sku, :string
    field :description, :string
    field :qty, :integer
    field :price_from, :float
    field :price_to, :float
    field :ean, :string
    
    timestamps()
  end

  @fields ~w(tenant sku description qty price_from price_to ean)a

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @fields)
    |> validate_required(@fields)
    |> unique_constraint(:sku, name: :unique_sku_by_tenant)
    |> validate_format(:sku, ~r/\A[a-zA-Z0-9\-]+$/)
    |> validate_format(:ean, ~r/\A[0-9]+$/)
    |> validate_length(:ean, min: 8, max: 13)
    |> check_constraint(:price_from, name: :price_from_must_be_positive)
    |> check_constraint(:price_to, name: :price_to_must_be_positive)
    |> check_constraint(:price_from, name: :price_reduction, message: "must be greater than price_to")
  end

  def all(tenant, page, per_page) do
    Product
    |> with_tenant(tenant)
    |> paginate(page, per_page)
    |> Repo.all()
  end

  def find_by_sku(tenant, sku) do
    Product
    |> with_tenant(tenant)
    |> with_sku(sku)
    |> Repo.one()
  end

  def insert(params) do
    %Product{}
    |> Product.changeset(params)
    |> Repo.insert()    
  end

  def update(product, params) do
    product
    |> Product.changeset(params)
    |> Repo.update()
  end

  def delete!(product),
    do: Repo.delete!(product)
    
  defp with_tenant(query, tenant) do
    from w in query,
      where: w.tenant == ^tenant
  end

  defp with_sku(query, sku) do
    from w in query,
      where: w.sku == ^sku
  end

  defp paginate(query, page, per_page) do
    from query,
      limit: ^per_page,
      offset: ^((page - 1) * per_page)
  end

end