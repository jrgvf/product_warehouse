defmodule ProductWarehouse.Router do
  use ProductWarehouse.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :api_authenticated do
    plug :accepts, ["json"]
    plug ProductWarehouse.API.Authentication
  end

  scope "/", ProductWarehouse do
    pipe_through :api

    options "/api", OptionsController, :api_options
    get "/api", OptionsController, :api_options
  end


  scope "/api", ProductWarehouse do
    pipe_through :api_authenticated

    resources "/products", ProductController, except: [:new, :edit], param: "sku"
  end
end
