defmodule ProductWarehouse.OptionsController do
  use ProductWarehouse.Web, :controller

  def api_options(conn, _params) do
    render(conn, "api_options.json", api_url: api_url(conn))
  end

  defp api_url(conn) do
    base_url = 
      conn
      |> current_url()
      |> String.split("/api")
      |> Enum.fetch!(0)

    base_url <> "/api"
  end
  
end