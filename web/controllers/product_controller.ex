defmodule ProductWarehouse.ProductController do
  use ProductWarehouse.Web, :controller

  alias ProductWarehouse.{Product}

  def action(conn, _) do
    params = Map.put(conn.params, "tenant", conn.assigns.tenant)
    args = [conn, params]
    apply(__MODULE__, action_name(conn), args)
  end

  def index(conn, %{"tenant" => tenant} = params) do
    page = String.to_integer(Map.get(params, "page", "1"))
    per_page = String.to_integer(Map.get(params, "per_page", "10"))

    products = Product.all(tenant, page, per_page)
    render(conn, "index.json", products: products, next_url: build_next_url(conn, page, per_page))
  end

  def show(conn, %{"tenant" => tenant, "sku" => sku}) do
    with product = %Product{} <- Product.find_by_sku(tenant, sku) do
      render(conn, "product.json", product: product)
    else
      nil ->
        conn
        |> put_status(404)
        |> render("404.json", error: "Product not found")
    end
  end

  def create(conn, params) do
    with {:ok, product} <- Product.insert(params) do
      conn
      |> put_status(201)
      |> render("product.json", product: product)
    else
      {:error, %{errors: errors}} ->
        conn
        |> put_status(422)
        |> render("422.json", errors: errors)
    end
  end

  def delete(conn, %{"tenant" => tenant, "sku" => sku}) do
    with product = %Product{} <- Product.find_by_sku(tenant, sku) do
      Product.delete!(product)

      conn
      |> put_status(204)
      |> send_resp(:no_content, "")
    else
      nil ->
        conn
        |> put_status(404)
        |> render("404.json", error: "Product not found")
    end
  end

  def update(conn, %{"tenant" => tenant, "sku" => sku} = params) do
    with product = %Product{} <- Product.find_by_sku(tenant, sku) do
      with {:ok, product} <- Product.update(product, params) do
        render(conn, "product.json", product: product)
      else
        {:error, %{errors: errors}} ->
          conn
          |> put_status(422)
          |> render("422.json", errors: errors)
      end
    else
      nil ->
        conn
        |> put_status(404)
        |> render("404.json", error: "Product not found")
    end
  end

  defp build_next_url(conn, page, per_page),
    do: base_url(conn) <> "?" <> URI.encode_query(query_params(page, per_page))

  defp base_url(conn), 
    do: conn |> current_url() |> String.split("?") |> Enum.fetch!(0)

  defp query_params(page, per_page),
    do: %{page: page + 1, per_page: per_page}
  
end