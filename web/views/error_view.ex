defmodule ProductWarehouse.ErrorView do
  use ProductWarehouse.Web, :view

  def render("404.json", _assigns),
    do: %{errors: [%{message: "Page not found"}]}

  def render("422.json", _assigns),
    do: %{errors: [%{message: "Bad request"}]}

  def render("500.json", _assigns),
    do: %{errors: [%{message: "Internal server error"}]}

  def render("401.json", %{error: error_message}),
    do: %{errors: [%{message: error_message}]}

  def render("401.json", _assigns),
    do: %{errors: [%{message: "Unauthorized request"}]}

  # In case no render clause matches or no
  # template is found, let's render it as 500
  def template_not_found(_template, assigns),
    do: render "500.json", assigns

end
