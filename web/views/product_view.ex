defmodule ProductWarehouse.ProductView do
  use ProductWarehouse.Web, :view

  def render("index.json", %{products: products, next_url: next_url}) do
    %{
      products: render_many(products, __MODULE__, "product.json"),
      next_url: next_url
    }
  end

  def render("product.json", %{product: product}) do
    %{
      sku: product.sku,
      description: product.description,
      qty: product.qty,
      price_from: product.price_from,
      price_to: product.price_to,
      ean: product.ean,
      inserted_at: product.inserted_at,
      updated_at: product.updated_at
    }
  end

  def render("404.json", %{error: error_message}) do
    %{errors: [%{message: error_message}]}
  end

  def render("422.json", %{errors: errors}) do
    errors = Enum.map(errors, fn {field, detail} ->
      %{attribute: field, message: render_error_message(detail)}
    end)

    %{errors: errors}
  end

  defp render_error_message({message, values}),
    do: Enum.reduce(values, message, fn ({key, value}, acc) -> safe_replace(acc, key, value) end) |> String.capitalize()

  defp render_error_message(message),
    do: to_string(message) |> String.capitalize()

  defp safe_replace(acc, _key, value) when is_tuple(value),
    do: acc

  defp safe_replace(acc, key, value),
    do: String.replace(acc, "%{#{key}}", to_string(value))

end
