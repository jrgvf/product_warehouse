defmodule ProductWarehouse.OptionsView do
  use ProductWarehouse.Web, :view

  def render("api_options.json", %{api_url: api_url}) do
    %{
      api_url: api_url,
      observation: "All requests need a 'api-key' Header. Fill with same value for all requests.",
      resources: resources(api_url)
    }
  end

  defp resources(api_url) do
    %{
      index_products: index_products(api_url),
      get_product: get_product(),
      create_product: create_product(),
      update_product: update_product(),
      remove_product: remove_product()
    }
  end

  defp index_products(api_url) do
    %{
      method: "GET",
      path: "/products",
      description: "Index of products",
      request_body: nil,
      responses: [
        %{
          type: :success,
          status: 200,
          response_body: %{
            products: [
              %{
                updated_at: "2018-07-10T14:15:54.039241Z",
                sku: "teste1",
                qty: 10,
                price_to: 9.99,
                price_from: 20,
                inserted_at: "2018-07-10T14:15:54.034723Z",
                ean: "1234567890123",
                description: "teste"
              }
          ],
            next_url: "#{api_url}?page=2&per_page=10"
          }
        }
      ]
    }
  end

  defp get_product do
    %{
      method: "GET",
      path: "/products/:sku",
      description: "Get product",
      request_body: nil,
      responses: [
        %{
          type: :success,
          status: 200,
          response_body: %{
            updated_at: "2018-07-10T14:15:54.039241Z",
            sku: "teste1",
            qty: 20,
            price_to: 9.99,
            price_from: 20,
            inserted_at: "2018-07-10T15:15:54.034723Z",
            ean: "1234567890123",
            description: "teste"
          }
        },
        %{
          type: :error,
          status: 404,
          response_body: %{
            errors: [
              %{message: "Product not found"}
            ]
          }
        }
      ]
    }
  end

  defp create_product do
    %{
      method: "POST",
      path: "/products",
      description: "Product creation",
      observation: "All attributes are required",
      request_body: %{
        sku: "teste1",
        qty: 10,
        price_to: 9.99,
        price_from: 20,
        ean: "1234567890123",
        description: "teste"
      },
      responses: [
        %{
          type: :success,
          status: 201,
          response_body: %{
            updated_at: "2018-07-10T14:15:54.039241Z",
            sku: "teste1",
            qty: 10,
            price_to: 9.99,
            price_from: 20,
            inserted_at: "2018-07-10T14:15:54.034723Z",
            ean: "1234567890123",
            description: "teste"
          }
        },
        %{
          type: :error,
          status: 422,
          response_body: %{
            errors: [
              %{message: "Has already been taken", attribute: "sku"}
            ]
          }
        }
      ]
    }
  end

  defp update_product do
    %{
      method: "PUT OR PATCH",
      path: "/products/:sku",
      description: "Product update",
      request_body: %{
        qty: 20,
        price_to: 9.99,
        price_from: 20,
        ean: "1234567890123",
        description: "teste"
      },
      responses: [
        %{
          type: :success,
          status: 200,
          response_body: %{
            updated_at: "2018-07-10T14:15:54.039241Z",
            sku: "teste1",
            qty: 20,
            price_to: 9.99,
            price_from: 20,
            inserted_at: "2018-07-10T15:15:54.034723Z",
            ean: "1234567890123",
            description: "teste"
          }
        },
        %{
          type: :error,
          status: 422,
          response_body: %{
            errors: [
              %{message: "Must be greater than price_to", attribute: "price_from"}
            ]
          }
        },
        %{
          type: :error,
          status: 404,
          response_body: %{
            errors: [
              %{message: "Product not found"}
            ]
          }
        }
      ]
    }
  end

  defp remove_product do
    %{
      method: "DELETE",
      path: "/products/:sku",
      description: "Remove product",
      request_body: nil,
      responses: [
        %{
          type: :success,
          status: 204,
          response_body: nil
        },
        %{
          type: :error,
          status: 404,
          response_body: %{
            errors: [
              %{message: "Product not found"}
            ]
          }
        }
      ]
    }
  end
end