defmodule ProductWarehouse.API.Authentication do
  import Plug.Conn
  alias ProductWarehouse.ErrorView

  def init(options),
    do: options

  def call(conn, _options) do
    case get_req_header(conn, "api-key") do
      [api_key] -> verify(conn, api_key)
      _ -> unauthorized(conn)
    end
  end

  defp verify(conn, api_key) when api_key in ["", nil],
    do: unauthorized(conn)

  defp verify(conn, api_key),
    do: assign(conn, :tenant, api_key)

  defp unauthorized(conn, message \\ "Header api-key not informed") do
    conn
    |> put_resp_header("content-type", "application/json")
    |> send_resp(401, Poison.encode!(ErrorView.render("401.json", error: message)))
    |> halt()
  end
end